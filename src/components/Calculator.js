import React, { useState, useEffect, useRef } from "react";
import "../Calculator.css";

const Calculator = () => {
    const [result, setResult] = useState("");
    const inputRef = useRef(null);

    useEffect(() => inputRef.current.focus());

    const handleClick = (e) => {
        setResult(result.concat(e.target.name));
    }

    const backspace = () => {
        setResult(result.slice(0, -1));
    }

    const clear = () => {
        setResult("");
    }

    const calculate = () => {
        try {
            setResult(eval(result).toString());
        } catch (error) {
            setResult("Error");
        }
    }

    return (
        <div className="calc-app">
            <form>
                <input data-testid="output" type="text" value={result} ref={inputRef}/>
            </form>

            <div className="keypad">
                <button id="clear" onClick={clear}>Clear</button>
                <button id="backspace" onClick={backspace}>C</button>
                <button name="+" onClick={handleClick} data-testid="add">+</button>
                <button name="7" onClick={handleClick} data-testid="7">7</button>
                <button name="8" onClick={handleClick} data-testid="8">8</button>
                <button name="9" onClick={handleClick} data-testid="9">9</button>
                <button name="-" onClick={handleClick} data-testid="sub">-</button>
                <button name="4" onClick={handleClick} data-testid="4">4</button>
                <button name="5" onClick={handleClick} data-testid="5">5</button>
                <button name="6" onClick={handleClick} data-testid="6">6</button>
                <button name="*" onClick={handleClick} data-testid="mul">&times;</button>
                <button name="1" onClick={handleClick} data-testid="1">1</button>
                <button name="2" onClick={handleClick} data-testid="2">2</button>
                <button name="3" onClick={handleClick} data-testid="3">3</button>
                <button name="/" onClick={handleClick} data-testid="div">/</button>
                <button name="0" onClick={handleClick} data-testid="0">0</button>
                <button name="." onClick={handleClick} data-testid=".">.</button>
                <button id="result" onClick={calculate} data-testid="result">Result</button>
            </div>

        </div>
    )
};

export default Calculator;