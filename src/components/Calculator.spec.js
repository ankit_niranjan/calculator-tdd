import React from 'react';
import Enzyme from "enzyme";
import { shallow } from "enzyme/build";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Calculator from './Calculator';
import { render, fireEvent } from "@testing-library/react";

Enzyme.configure({ adapter: new Adapter() });

describe("Basic rendering of calculator", () => {
    it("should render result Value", () => {
        const result = shallow(<Calculator/>);

        const resultValue = result.find("#output");

        expect(resultValue).toBeDefined();
    });

    it("should Value of result equal to empty string", () => {
        const component = render(<Calculator/>);

        expect(component.getByTestId("output")).toHaveTextContent("");
    });
});

describe("Testing Calculator Functionality", () => {
    it("should return 3 when 1 and 2 is added", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("1"));
        fireEvent.click(getByTestId("add"));
        fireEvent.click(getByTestId("2"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("3");
    });

    it("should return 1 when 3 and 2 is subtracted", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("3"));
        fireEvent.click(getByTestId("sub"));
        fireEvent.click(getByTestId("2"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("1");
    });

    it("should return 16 when 2 and 8 is multiplied", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("2"));
        fireEvent.click(getByTestId("mul"));
        fireEvent.click(getByTestId("8"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("16");
    });

    it("should return 8 when 16 is divided by 8", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("1"));
        fireEvent.click(getByTestId("6"))
        fireEvent.click(getByTestId("div"));
        fireEvent.click(getByTestId("2"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("8");
    });

    it("should return 51 when input is 9*6-7+8/2", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("9"));
        fireEvent.click(getByTestId("mul"));
        fireEvent.click(getByTestId("6"));
        fireEvent.click(getByTestId("sub"));
        fireEvent.click(getByTestId("7"));
        fireEvent.click(getByTestId("add"));
        fireEvent.click(getByTestId("8"));
        fireEvent.click(getByTestId("div"));
        fireEvent.click(getByTestId("2"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("51");
    });

    it("should return Error when expression is Invalid", () => {
        const { getByTestId } = render(<Calculator/>);

        fireEvent.click(getByTestId("9"));
        fireEvent.click(getByTestId("mul"));
        fireEvent.click(getByTestId("result"));

        expect(getByTestId("output")).toHaveValue("Error");
    });
});